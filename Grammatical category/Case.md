Відмінок (case)

[[Grammatical category]]

- nominative (називний)
- genitive (родовий)
- dative (давальний)
- accusative (знахідний)
- instrumental (орудний)
- locative (місцевий)
- vocative (клична форма)
