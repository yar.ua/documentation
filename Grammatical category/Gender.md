Gender (рід)

[[Grammatical category]]

- masculine (чоловічий)
- feminine (жіночий)
- neuter (середній)
- common (спільний)
