Оживленість

[[Grammatical category]]

Mainly manifests itself only in accusative [[case]].

- inanimate (неістота)
- animate (істота)
