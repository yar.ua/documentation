Кількісний числівник

[[Part of speech]]

E.g. "оди́н"

Grammatically similar to [[Noun]].
Has following grammatical categories:
- [[Case]]
- [[Gender]]
- [[Animacy]]