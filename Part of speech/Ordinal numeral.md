Порядковий числівник

[[Part of speech]]

E.g. "пе́рший"

Grammatically similar to [[Adjective]].
Has following grammatical categories:
- [[Case]]
- [[Gender]]
- [[Animacy]]
- [[Number]]
